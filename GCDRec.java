Gpublic class GCDRec {

    static int gcd(int x,int y)
    {

        int z = (x / y);
        int t = x - (z * y);

        if(t==0)
            return y;
        else
            return gcd(y, t);

    }

    public static void main(String[] args)
    {

        int numberone = Integer.parseInt(args[0]);
        int numbertwo = Integer.parseInt(args[1]);
        System.out.println(gcd(numberone,numbertwo));

    }
}
