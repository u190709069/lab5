import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FindSequence {

    public static void main(String[] args) throws FileNotFoundException{
        int matrix[][] = readMatrix();
        String array[]= new String[10];
        array[9]=" ";
        boolean found = false;
        found: for (int i=0; i< matrix.length; i++) {
            for (int j=0; j < matrix[i].length; j++) {
                if (search(0,matrix, i,j,array)){
                    found = true;
                    break found;
                }
            }
        }

        if (found) {
            System.out.println("A sequence is found");
        }


        for(int i=0;i<10;i++){
            int row= array[i].charAt(0);
            int row1 = Character.getNumericValue(row);
            int col= array[i].charAt(2);
            int col1 = Character.getNumericValue(col);
            matrix[row1][col1]=9-matrix[row1][col1];

        }

        printMatrix(matrix);
    }

    private static boolean search (int number, int[][]matrix, int row, int col,String[] array) {

        if(number==9 && matrix[row][col]==9){
            if(array[9]==" ")
           array[number]= row+" "+col+" "+number;


           return true;


        }
        else if(matrix[row][col]==number) {
            if(array[9]==" ")
            array[number]= row+" "+col+" "+number;
            if (row != 0)
                search(number + 1, matrix, row - 1, col,array);
            if (row != 9)
                search(number + 1, matrix, row + 1, col,array);
            if (col != 0)
                search(number + 1, matrix, row, col - 1,array);
            if (col != 9)
                search(number + 1, matrix, row, col + 1,array);




        }

        return false;

    }

    private static int[][] readMatrix() throws FileNotFoundException{
        int[][] matrix = new int[10][10];
        File file = new File("matrix.txt");

        try (Scanner sc = new Scanner(file)){


            int i = 0;
            int j = 0;
            while (sc.hasNextLine()) {
                int number = sc.nextInt();
                matrix[i][j] = number;
                if (j == 9)
                    i++;
                j = (j + 1) % 10;
                if (i == 10)
                    break;
            }
        } catch (FileNotFoundException e) {
            throw e;
        }
        return matrix;
    }

    private static void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j]+" ");
            }
            System.out.println();
        }
    }
}