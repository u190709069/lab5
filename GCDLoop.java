public class GCDLoop {

    public static void main(String[] args)
    {

        int x = Integer.parseInt(args[0]);
        int y = Integer.parseInt(args[1]);

        int z = (x/y);
        int t = x-(z*y);


        while(t!=0)
        {
            z = (x / y);
            t = x - (z * y);
            if (t == 0)
                break;


                x = y;
                y = t;

        }

        System.out.println(y);

    }
}
